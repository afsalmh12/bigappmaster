#change directory
cd project_directory
# To install all dependencies

$ npm install

# update the database url in server/db_layer/db_config file/mongo.js

#To run the project
$ node server

### API - Postman

API to do registartion
----------------------

url : http://localhost:8080/newuser/signup
methord : Post
params : email,password,fullName,dob,role

eg : http://localhost:8080/newuser/signup?email=test@gmail.com&password=test1234&fullName=Test&dob=09/09/1990&role=tester

Api to do login
---------------
url : http://localhost:8080/signin/UserAuthentication
methord : Get
params : email,password

eg : http://localhost:8080/signin/UserAuthentication?email=afsalmh12@gmail.com&password=3333

please save token from response

API to check paranthesis
-------------------------
url : http://localhost:8080/balanced/verifyParanthesis
methord : Get
params : paranthesis
headers : {authorization:token}

eg:http://localhost:8080/balanced/verifyParanthesis?paranthesis=[]]]

Api to list the user list
--------------------------
url : http://localhost:8080/user/getAllUsers
methord : Get
headers : {authorization:token}

//please login as user whoes role is "Admin" and get the token to perform this action

http://localhost:8080/user/getAllUsers

Api to remove user
--------------------
url : http://localhost:8080/user/removeUser
methord : Get
params : email
headers : {authorization:token}

//please login as user whoes role is "Admin" and get the token to perform this action

http://localhost:8080/user/removeUser?email=test@gmail.com