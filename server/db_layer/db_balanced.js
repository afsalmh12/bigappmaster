const balanced = require('../models/balanced');
const attampt = require('../models/attempts');
const Promise = require("promise");

//function to save balanced string in db
function saveBalanced(balancedObj) {
    return new Promise(function (resolve, reject) {
        let balancedData = new balanced(balancedObj);
        balancedData.save()
            .then((data) => {
                console.log(data);
                resolve("Success")
            })
            .catch((err) => {
                console.log(err);
                reject(err);
            })
    })
}

//function to saving first attempt of a user in db
function addAttempt(attamptObj){
    return new Promise(function (resolve, reject) {
        let attamptData = new attampt(attamptObj);
        attamptData.save()
            .then((data) => {
                console.log(data);
                resolve("Success")
            })
            .catch((err) => {
                console.log(err);
                reject(err);
            })
    })
}

//function get the total attempts of a user

function getAttamptNumber(email){
    return new Promise(function (resolve, reject) {
        attampt.findOne({ email: email }, 'email totalAttempts').exec(
            function (err, result) {
                if (err) reject(err);
                else {
                    if (result) {
                        resolve(result.totalAttempts);
                    }else{
                        resolve(null);
                    }    
                }
            })
    })
}

//function to update user attempts
function updateAttempts(email,attamptNo){
    return new Promise(function (resolve, reject) {
        attampt.update({ email: email },{totalAttempts:attamptNo}).exec(
            function (err, result) {
                if (err) reject(err);
                else {
                   resolve("Success");
                }
            })
    })
}

module.exports = { 'saveBalanced': saveBalanced ,'addAttempt' : addAttempt , 'getAttamptNumber':getAttamptNumber,'updateAttempts':updateAttempts};