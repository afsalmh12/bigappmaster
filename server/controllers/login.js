const express = require("express");
const router = express.Router();
const userLogin = require('../db_layer/db_user');
const dotenv = require('dotenv').config();
var jwt = require('jsonwebtoken');

router.get('/UserAuthentication', function (req, res) {
    let emailId = req.query.email;
    let password = req.query.password;
    async function UserAuthentication() {
        //checking user credentials
        var result = await userLogin.validUser(emailId, password);
        if (result) {
            //creating token for the user
            var token = jwt.sign({ id: result.email }, process.env.superKey, {
                expiresIn: 3600 // expires in 1 hours
              });
            res.json({token:token,message:"Success"});
        } else {
            res.send("Invalid credentials");
        }
    }
    UserAuthentication(); 
});
module.exports = router;