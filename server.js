const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const signup = require('./server/controllers/signup');
const login = require('./server/controllers/login');
const mongo = require('./server/db_layer/db_config/mongo');
const jwtVerifier = require('./server/jwt/jwtVerifier');
const balanced = require('./server/controllers/balanced');
const user = require('./server/controllers/user');
const port=8080;
//creating express app
const app = express();
//body-parser as middleware
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use('/newuser',signup);
app.use('/signin',login);
//verify token using function jwtVerifier.verifyJwtToken
app.use('/balanced',jwtVerifier.verifyJwtToken,balanced);
app.use('/user',jwtVerifier.verifyJwtToken,user);
app.get('*',function(req,res){
res.send("Server started");
});
app.listen(port,function(){
    console.log("Server connected : "+port);
});